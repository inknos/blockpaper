#!/bin/bash
SOURCE_IMG_DIR=source-img
ROTATED_IMG_DIR=rotated
if [ -d "$ROTATED_IMG_DIR" ]; then rm -Rf $ROTATED_IMG_DIR; fi
mkdir $ROTATED_IMG_DIR
for num in {1..27}
do
    # generate number 1,2,3,4
    rnd=$(( $RANDOM %4 + 1))
    # pick a random image from source dir
    ls $SOURCE_IMG_DIR/rot*.png |sort -R |tail -$N | head -1 | while read file; do
        convert $file -rotate $(( 90*$rnd )) $ROTATED_IMG_DIR/$RANDOM.png;
    done
done
cp $SOURCE_IMG_DIR/gnu.png $ROTATED_IMG_DIR/$RANDOM.png
montage -geometry 300x300+10+10 -tile x4 -background '#111111' $ROTATED_IMG_DIR/*.png montage.png

gsettings set org.gnome.desktop.background picture-uri '/home/nsella/Documents/personal_projects/gnu-images/montage.png'
